import React, { useState } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import IconButton from '@material-ui/core/IconButton';
import Link from '@material-ui/core/Link';
import { texts } from '../../language/texts';
import { useStyles } from '../../themes/jss/theme';
import MainPage from './../pages/main';
import ContactsPage from './../pages/contacts';
import ProjectPage from './../pages/project';
import logo from './../../images/logo.png';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        GC Viewer
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

export default function Album() {
  const classes = useStyles();
  const [curLangCode, setCurLangCode] = useState( "L1" );
  const [page, setPage] = useState( 0 );

  const changeLanguage = ( langCode ) => {
    setCurLangCode( langCode );
  }

  const changePage = ( pageCode ) => {
    setPage( pageCode );
  }

  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar position="relative">
        <Toolbar style={{ backgroundColor: "#2B3642" }}>
          <div style={{ flexGrow: 1 }}>
            {/* Home */}
            <IconButton style={{ padding: 10 }} color="inherit" onClick={ () => changePage( 0 ) } > 
              <img src={ logo } width="32" height="32" alt="gc-logo"/>
            </IconButton>
            {/* Project */}
            <Button className={ classes.gcButton } size="small" color="inherit" onClick={ () => changePage( 2 ) } >{ texts[curLangCode][5] }</Button>
            {/* About us */}
            <Button className={ classes.gcButton } size="small" color="inherit" onClick={ () => changePage( 1 ) } >{ texts[curLangCode][4] }</Button>
          </div>
          <div>
            {/* Languages */}
            <Button className={ classes.gcButton } size="small" color="inherit" onClick={ () => changeLanguage( "L1" ) } >IT</Button>
            <Button className={ classes.gcButton } size="small" color="inherit" onClick={ () => changeLanguage( "L2" ) } >EN</Button>
          </div>
        </Toolbar>
      </AppBar>
      <main>
        {/* Hero unit */}
        { page === 0 &&
          <div className={classes.heroContent}>
            <Container maxWidth="lg">
              <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                { texts[curLangCode][0] }
              </Typography>
              <Typography component="span" variant="body1" align="center" color="textSecondary" paragraph>
                { texts[curLangCode][1] }
              </Typography>
            </Container>
          </div>
        }
        <Container className={classes.cardGrid} maxWidth="lg">
          { page === 0 && <MainPage curLangCode={ curLangCode }/> }
          { page === 1 && <ContactsPage curLangCode={ curLangCode }/> }
          { page === 2 && <ProjectPage curLangCode={ curLangCode }/> }
        </Container>
      </main>
      {/* Footer */}
      <footer className={classes.footer}>
        <Copyright />
      </footer>
      {/* End footer */}
    </React.Fragment>
  );
}
