import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  modal: {
    position: "fixed",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%) scale(0)",
    animation: `$show .25s ease forwards`,
    background: "#000000",
    minHeight: "100%",
    minWidth: "100%"
  },
  ciframe: {
    width: "100%"
  },
  loading: {
    position: "absolute",
    width: "100%",
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    top: 0,
    left: 0
  },
  topbar: {
    display: "flex",
    width: "100%",
    height: 50,
    alignItems: "center"
  },
  "@keyframes show": {
    "to": {
      transform: "translate(-50%, -50%) scale(1)"
    }
  },
  close: {
    position: "absolute",
    right: 10,
    fontSize: "1.25rem",
    fontWeight: "bold",
    borderRadius: 20,
    "&:hover": {
      backgroundColor: "#ececec"
    }
  }
}));