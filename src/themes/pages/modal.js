import React, { useRef, useEffect, useState } from 'react';
import { createPortal } from 'react-dom';
import { useStyles } from '../../themes/jss/modal';
import CircularProgress from '@material-ui/core/CircularProgress';

export default function Modal ({ children, onClose, iFrameURL }) {
  let classes = useStyles();
  let ciframe = useRef( null );
  let main = useRef( null );

  const [loading, setLoading] = useState( true );

  window.addEventListener( "resize", ( e ) => {
    updateDimensions();
  } );

  const updateDimensions = () => {
    let containerEl = main.current;
    if( containerEl ) {
      containerEl.style.width = window.innerWidth + "px";
      containerEl.style.height = window.innerHeight + "px";
    }

    let iframeEl = ciframe.current
    if( iframeEl ) {
      iframeEl.style.width = window.innerWidth + "px";
      iframeEl.style.height = ( window.innerHeight - 50 ) + "px";
    }
  }

  const hideLoading = () => {
    setLoading( false );
  }

  useEffect(() => {
    updateDimensions();
  })

  return (
    createPortal(
      <div className={ classes.modal }>
        <div className={ classes.topbar }>
          <button onClick={ onClose } className={ classes.close }>&times;</button>
        </div>
        <div ref={ ciframe } className={ classes.ciframe } >
          <iframe onLoad={ hideLoading } frameBorder={ 0 } width="100%" height="100%" title="three-viewer" src={ iFrameURL }/>
          { loading &&
            <div className={ classes.loading }>
              <CircularProgress />
            </div>
          }
        </div>
      </div>,
    document.body)
  )
}