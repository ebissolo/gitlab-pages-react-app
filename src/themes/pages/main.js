import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Modal from './modal';
import { texts } from '../../language/texts';
import { useStyles } from '../../themes/jss/theme';

let currIFrameURL = "";

export default function MainPage( props ) {
  const classes = useStyles();
  const [openModal, setOpenModal] = useState( false );

  let cards = [
    {
      id: 0,
      imgUrl: "https://gc-viewer-server.herokuapp.com/public/embed/5d9ce14aab8bf1b33a658f73",
      imgTitle: texts[props.curLangCode].c1.t,
      txt: texts[props.curLangCode].c1.d,
    },
    {
      id: 1,
      imgUrl: "https://gc-viewer-server.herokuapp.com/public/embed/5d9ce14aab8bf1b33a658f73",
      imgTitle: texts[props.curLangCode].c2.t,
      txt: texts[props.curLangCode].c2.d,
    },
    {
      id: 2,
      imgUrl: "https://gc-viewer-server.herokuapp.com/public/embed/5d9ce14aab8bf1b33a658f73",
      imgTitle: texts[props.curLangCode].c3.t,
      txt: texts[props.curLangCode].c3.d,
    }
  ];

  const handleOpenModal = ( url ) => {
    currIFrameURL = url;
    setOpenModal( true );
  }

  return (
    <div>
      <Grid container spacing={4}>
        {cards.map(card => (
          <Grid item key={card.id} xs={12} sm={6} md={4}>
            <Card className={classes.card}>
              <CardMedia
                className={classes.cardMedia}
                image="https://source.unsplash.com/random"
                title={ card.imgTitle }
              />
              <CardContent className={classes.cardContent}>
                <Typography gutterBottom variant="h5" component="h2">
                  { card.imgTitle }
                </Typography>
                <Typography>
                  { card.txt }
                </Typography>
              </CardContent>
              <CardActions>
                <Button size="small" color="primary" onClick={ () => handleOpenModal( card.imgUrl ) }>
                  View
                </Button>
              </CardActions>
            </Card>
          </Grid>
        ))}
      </Grid>
      { console.log( "render: ", currIFrameURL ) }
      { openModal &&
        <Modal open={ openModal } onClose={ () => setOpenModal( false ) } iFrameURL={ currIFrameURL } />
      }
    </div>
  )
}