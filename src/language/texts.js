import React from 'react';

export const texts = {
 "L1": {
    "0": "GC Viewer Demo",
    "1": <div><b>Benvenuto!</b><br/><br/>GCView consente di mostrare eventi in un ambiente spazio-temporale, usando l'asse Z (altezza) come asse temporale. In questo contesto è possibile mostrare qualsiasi tipo di evento, ad esempio eventi storici o recenti, come lo scatto di una foto, ma anche eventi personali, come il momento della nascita e altri eventi familiari di persone specifiche. L'idea che guida questo progetto è quella di utilizzare questo ambiente per custodire la memoria delle comunità, collegando la vita personale delle persone con eventi (pre) storici che si verificano nello stesso luogo, nel lungo flusso del tempo.<br/>GCView è un progetto culturale di raccolta e catalogazione di informazioni, e di ricerca sulle modalità di rappresentazione di fenomeni complessi. I dati visualizzati sono raccolti in progetti. Ogni progetto consiste di una raccolta di dati spazio-temporali focalizzati su un tema specifico, e una visualizzazione di questi dati.<br/><br/><b>Ti invitiamo ad esplorare i progetti inseriti, e a farci sapere che cosa ne pensi, o se ti viene in mente qualche progetto interessante che possiamo sviluppare insieme.</b></div>,
    "2": "Siamo noi",
    "3": "Home",
    "4": "Chi Siamo",
    "5": "Progetto",
    "c1": {
       t: "card1IT",
       d: "descrizione card1IT"
    },
    "c2": {
      t: "card2IT",
      d: "descrizione card2IT"
   },
   "c3": {
      t: "card3IT",
      d: "descrizione card3IT"
   },
 },
 "L2": {
    "0": "GC Viewer Demo",
    "1": <div><b>Welcome!</b><br/><br/>GCView allows to show events in a space-time environment, using Z-axis (height) as temporal axis. In this frame it's possible to show any kind of events, i.e. historical ones or recent ones, like the shoot of a photo, but also personal events, like birth and familiar events of specific people. The idea is to use this frame to save community memories, linking personal life of people with (pre)historical events that are happened in the same place, in different times.<br/>GCView is a cultural project for the collection of information and data, and for research on how to represent complex phenomena. Data are collected in projects.<br/>Every project consists in a collection of spatio-temporal data focused on a specific theme, and a visualization of these data.<br/><br/><b>We invite you to explore the projects below, and let us know what you think, or if you come up with some interesting projects that we can develop together.</b></div>,
    "2": "We are",
    "3": "Home",
    "4": "About us",
    "5": "Project",
    "c1": {
      t: "card1EN",
      d: "description card1EN"
    },
    "c2": {
      t: "card2EN",
      d: "description card2EN"
    },
    "c3": {
      t: "card3EN",
      d: "description card3EN"
    }
  }
};